package com.devcamp.jbrs10.bookauthorapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getListBook() {
        Author aut1 = new Author("Ngạn", "ngan@mail", 'm');
        Author aut2 = new Author("Tri", "tri@mail", 'm');
        Author aut3 = new Author("Huyen", "huyen@mail", 'f');
        Author aut4 = new Author("ABC", "abc@mail", 'f');
        Author aut5 = new Author("XYZ", "xyz@mail", 'm');
        Author aut6 = new Author("HUI", "hui@mail", 'f');
        // System.out.println(aut1);
        // System.out.println(aut2);
        // System.out.println(aut3);
        // System.out.println(aut4);
        // System.out.println(aut5);
        // System.out.println(aut6);

        ArrayList<Author> authorlist1 = new ArrayList<>();
        authorlist1.add(aut1);
        authorlist1.add(aut2);
        ArrayList<Author> authorlist2 = new ArrayList<>();
        authorlist2.add(aut3);
        authorlist2.add(aut4);
        ArrayList<Author> authorlist3 = new ArrayList<>();
        authorlist3.add(aut5);
        authorlist3.add(aut6);
        Book bk1 = new Book("Truyện Ma", authorlist1, 150000);
        Book bk2 = new Book("Truyện Cười", authorlist2, 90000, 16);
        Book bk3 = new Book("Doremon", authorlist3, 30000, 100);
        System.out.println(bk1.toString());
        System.out.println(bk2.toString());
        System.out.println(bk3.toString());

        ArrayList<Book> listBook = new ArrayList<>();
        listBook.add(bk1);
        listBook.add(bk2);
        listBook.add(bk3);

        return listBook;
    }
}
