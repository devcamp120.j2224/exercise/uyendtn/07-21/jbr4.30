package com.devcamp.jbrs10.bookauthorapi;

import java.util.ArrayList;

public class Book {
    private String name;
    private ArrayList<Author> authors;
    private double price;
    private int qty = 0;

    public Book(String name, ArrayList<Author> authors, double price, int qty) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qty = qty;
    }
    
    public Book(String name, ArrayList<Author> authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public String getName() {
        return name;
    }


    public ArrayList<Author> getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getAuthorNames(){  
        String authorName = "";      
        for (int i = 0; i < authors.size(); i++){            
            authorName += authors.get(i).getName() + " ";            
        }
        return authorName;
    }

    @Override
    public String toString() {
        return "Book [name=" + this.name + ", authors=" + this.authors + ", price=" + this.price + ", qyt=" + this.qty;
    }
    
}
